

# Hierarchy

**FrequencyObservableOptions**

# Properties

<a id="provider"></a>

## `<Optional>` provider

**● provider**: *`any`*

*Defined in [types.ts:48](https://github.com/paritytech/js-libs/blob/ae9ea03/packages/light.js/src/types.ts#L48)*

___

