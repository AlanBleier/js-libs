# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [5.0.0](https://github.com/paritytech/js-libs/tree/master/packages/electron/compare/v4.1.1...v5.0.0) (2019-03-05)

**Note:** Version bump only for package @parity/electron





# [4.0.0](https://github.com/paritytech/js-libs/tree/master/packages/electron/compare/v3.0.31...v4.0.0) (2019-01-22)

**Note:** Version bump only for package @parity/electron
